$(document).ready(function () {

    //hamburger
    $('#hamburger').click(function () {
        $(this).toggleClass('open');
        $('#nav').slideToggle('slow');

    });
//search toggle
    $('.jsSearchToggle').click(function () {
        $(this).parents('.search').addClass('active');

    });

    $(document).click(function (e) {
        var div = $('.search');
        // var div2 = $('#hamburger');

        if (!div.is(e.target) && div.has(e.target).length === 0) {
            $('.search').removeClass('active');
        }

    });

//lang-switch
    $('.jsLangswitch').click(function () {
        $(this).find('.lang-switch__dropdown').slideToggle('fast');

    });

    $('.lang-switch__dropdown')
        .mousedown(function () {
            $('.lang-switch__dropdown').show();
        })
        .mouseleave(function () {
            $('.lang-switch__dropdown').hide();
        });
//slider
    $('.slider-for').slick({
        slidesToShow: 1, slidesToScroll: 1, arrows: true, fade: true, asNavFor: '.slider-nav',
        nextArrow: '<div class="slick-arrow-right"><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="30" viewBox="0 0 16 30"><g><g transform="translate(-842 -443)"><image width="16" height="30" transform="translate(842 443)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAeCAYAAAAl+Z4RAAAAvElEQVRIS6XVPQ7CMAxA4WchcTQGJHbExM2QWFi4A8wsnKULq1ELrSjNj+1kf18cKVFEVZ/AC9iKSIdziaqegCPwADZepAdWwBk4RBDpJ25BBqAFmYAoMgMiyALwIknAg2QBK1IELEgVqCEmoISYgSzifHz/1/7umuA7xRq4Ajvg5gJU9Tf+PH/rEZKxSGcCcnG/eRUoxVWgFhcBS5wFrHES8MQLwBvPgEg8AdF4AFriEbgA+8ivNAJNn+sb0qGvrvq0H9wAAAAASUVORK5CYII="/></g></g></svg></span></div>',
        prevArrow: '<div class="slick-arrow-left"><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="30" viewBox="0 0 16 30"><g><g transform="translate(-412 -443)"><image width="16" height="30" transform="translate(412 443)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAeCAYAAAAl+Z4RAAAAxklEQVRIS6XVOw7CMBCE4X9bGm5DDXeg5mxUiB6JgiLcCykKSsARiV87Jv18dmLPxmh4hmHYAndgY2r+G34AO+AsAavwBTi5gWTYrHcBufD4+lWgFK4CtXAR8ISzgDecBJRwBKjhBdASnoHW8AT8Ew7AE9gDn7tt1isFG3fQAQfgBhzN7KUCY7dDPWVk6sLqO0jIXKZWZNHGFiSqs4ok54GCZAeKFylOJA+ijrToiKtA7Z64gBLiBrKIUpwEcpV2EBb7/bm+AeHBqvosRZD5AAAAAElFTkSuQmCC"/></g></g></svg></span></div>',

    });
    $('.slider-nav').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        focusOnSelect: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    vertical: false,
                    slidesToShow: 4,
                    // slidesToScroll: 2
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

// .jsToggleList
    $('.jsToggleList').click(function () {
        $(this).toggleClass('open');
        $(this).next('.route__list').slideToggle('300');

    });
// //equal hieght
    ;(function ($, window, document, undefined) {
        'use strict';

        var $list = $('.img-with-description-wrap'),
            $items = $list.find(".img-with-description__text"),
            setHeights = function () {
                $items.css('height', 'auto');

                var perRow = Math.floor($list.width() / $items.width());
                if (perRow == null || perRow < 2) return true;

                for (var i = 0, j = $items.length; i < j; i += perRow) {
                    var maxHeight = 0,
                        $row = $items.slice(i, i + perRow);

                    $row.each(function () {
                        var itemHeight = parseInt($(this).outerHeight());
                        if (itemHeight > maxHeight) maxHeight = itemHeight;
                    });
                    $row.css('height', maxHeight);
                }
            };

        setHeights();
        $(window).on('resize', setHeights);
        $list.find('img').on('load', setHeights);
    })(jQuery, window, document);


    ;(function ($, window, document, undefined) {
        'use strict';

        var $list = $('.product-wrap'),
            $items = $list.find(".product__description"),
            setHeights = function () {
                $items.css('height', 'auto');

                var perRow = Math.floor($list.width() / $items.width());
                if (perRow == null || perRow < 2) return true;

                for (var i = 0, j = $items.length; i < j; i += perRow) {
                    var maxHeight = 0,
                        $row = $items.slice(i, i + perRow);

                    $row.each(function () {
                        var itemHeight = parseInt($(this).outerHeight());
                        if (itemHeight > maxHeight) maxHeight = itemHeight;
                    });
                    $row.css('height', maxHeight);
                }
            };

        setHeights();
        $(window).on('resize', setHeights);
        $list.find('img').on('load', setHeights);
    })(jQuery, window, document);

// scroll
    $(".page-nav a, .booknow-wrap a").on('click', function (event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 50
            }, 800, function () {
                // window.location.hash = hash;
            });
        }
    });
    $(document).on("scroll", onScroll);
    function onScroll(event){
        var scrollPos = $(document).scrollTop();
        $('.page-nav a').each(function () {
            var currLink = $(this);
            var refElement = $(currLink.attr("href"));
            var aposition;
            var aparentWidth;
            var containerWidth;
            if (refElement.position().top - 50 <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                $('.page-nav a').parent().removeClass("active");
                currLink.parent().addClass("active");
                aparentWidth = $('.page-nav').width();
                containerWidth = $('.scrtabs-tabs-fixed-container').width();
                aposition = $('.page-nav').find('li.active').position();

                var left = aparentWidth - containerWidth;
                if(aposition.left > left){
                    $('.scrtabs-tabs-movable-container').css({"left": "-" + left +"px"});

                }
                else{
                    $('.scrtabs-tabs-movable-container').css({"left": "-" + aposition.left+"px"});
                }
            }
            else{
                currLink.parent().removeClass("active");
            }
        });
    }
// form styler
    $('input, .jsStyler').styler({});
//datepick
    $(function () {
        $('#jsPopupDatepicker').datepick();
    });
// side form
    $('.jsStage2trigger').click(function () {
        if ($(this).hasClass('changed')) {
            $(this).parents('.side-form').find('.stage2').slideDown(500);
        }
    });
    $('.jsStage3trigger').click(function () {
        if ($(this).hasClass('changed')) {
            $(this).parents('.side-form').find('.stage3').slideDown(500);
            $(this).parents('.side-form').find('.stage3 .side-form__input-title').addClass('jsInputTriger');
            $(this).parents('.side-form').find('.stage2 .side-form__input-title').addClass('jsInputTriger open');
            $(this).parents('.side-form').find('.stage3 .jq-selectbox').hide();
            $(this).parents('.side-form').find('.full-itinerary-list__title').addClass('jsInputTriger open');
        }
    });
    $('.side-form__input-title, .full-itinerary-list__title').click(function () {
        if ($(this).hasClass('jsInputTriger')) {
            $(this).toggleClass('open');
            $(this).parents('.side-form__input').find('.jq-selectbox').slideToggle();
            $(this).parents('.full-itinerary-list').find('.full-itinerary-list__ul').slideToggle();
        }
    });

// tabs
    $('.nav-tabs').scrollingTabs({
        refresh: true,
        scrollToTabEdge: true,
        enableSwiping: true,
        forceActiveTab: true
    });
    var colwidth
    $(window).on('load resize', function () {
        var colwidth = $('.sticky-nav-wrap').parents('.col-md-9').width();
        $('.sticky-nav-wrap').css('width', colwidth  );
    });
//fancybox
    $("[data-fancybox]").fancybox({});

// jsFormWrapToogle
    $('.jsFormWrapToogle').click(function () {
        $(this).toggleClass('open');
        $(this).next().slideToggle('300');

    });

// .jsHiddenTexarea
    $('.jsHiddenTextareaWrap .jsStyler').click(function () {
        var selectval = $( ".jsHiddenTextareaWrap select" ).val();
        if ( selectval === "1"){
            $('.jsHiddenTextareaWrap').find('.jsHiddenTextarea').slideDown('slow');
        }
        if ( selectval === "0"){
            $('.jsHiddenTextareaWrap').find('.jsHiddenTextarea').slideUp('slow');
            $('.jsHiddenTextareaWrap').find('textarea').val('');
        }
    });

  // .jsNeighborHeight
    var jsNeighborHeight
    $(window).on('load resize', function () {
        var jsNeighborHeight = $('.jsNeighborHeight').prev().height();



        $('.jsNeighborHeight').css('height', jsNeighborHeight);
    });



});
